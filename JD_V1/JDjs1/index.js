window.onload=function(){
    // 轮播图
    bananer()
    // 倒计时
    downTime()
}
var bananer = function(){
    // 轮播图
    var bananer=document.querySelector(".jd_bananer")
    // 屏幕宽度
    var width = bananer.offsetWidth;
    // 图片容器
    var imageBox = bananer.querySelector("ul:first-child")
    // 电容器
    var pointBox = bananer.querySelector("ul:last-child")
    // 点
    var points = pointBox.querySelectorAll("li")
    // 自动无缝轮播
    var index = 1
    var timer = setInterval(function(){
        index++
        // 加过度
        imageBox.style.transition = "all 0.2s"
        imageBox.style.webkiTransition = "all 0.2s"
        // 做位移
        imageBox.style.transform = "translateX("+(-index*width)+"px)"
        imageBox.style.webkiTransform = "translateX("+(-index*width)+"px)"
    },1000)
    imageBox.addEventListener("transitionend",function(){
        if(index >= 5){
            index = 1
            // 瞬间定位，清过度，做位移
            imageBox.style.transition = "none"
            imageBox.style.webkitTransition = "none"

            imageBox.style.transform = "translateX("+(-index*width)+"px)"
            imageBox.style.webkiTransform = "translateX("+(-index*width)+"px)"
        }else if(index<=0){
            index = 5;
            // 瞬间定位，清过度，做位移
            imageBox.style.transition = "none"
            imageBox.style.webkitTransition = "none"

            imageBox.style.transform = "translateX("+(-index*width)+"px)"
            imageBox.style.webkiTransform = "translateX("+(-index*width)+"px)"
        }
        setPiont()
    })
    var setPiont = function(){
        for(var i = 0;i<points.length;i++){
            var obj = points[i]
            obj.classList.remove("now")            
        }
        points[index-1].classList.add("now");
    }
}
var downTime = function(){
    // 倒计时时间
    var time = 2*60*60
    var spans = document.querySelectorAll(".time span")
    // 每一秒更新显示时间
    var timer = setInterval(function(){
        time--
        // 秒转换成时分秒
        var h = Math.floor(time/3600)
        var m = Math.floor(time%3600/60)
        var s = time%60
        // 转换成两位数时分秒
        // 时
        spans[0].innerHTML = Math.floor(h/10)
        spans[1].innerHTML = h%10
        // 分
        spans[3].innerHTML = Math.floor(m/10)
        spans[4].innerHTML = m%10
        // 秒
        spans[6].innerHTML = Math.floor(s/10)
        spans[7].innerHTML = s%10
        // 临界值判断
        if(time<=0){
            clearInterval(timer)
        }
    },1000)
}